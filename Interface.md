# 项目管理API接口文档

## 协议规范

### 接口架构
所有的接口均采用RESTful API，详情请见：

[http://www.ruanyifeng.com/blog/2018/10/restful-api-best-practices.html](http://www.ruanyifeng.com/blog/2018/10/restful-api-best-practices.html)


### 复杂数据传输
为方便后端处理，对于较为复杂的参数（属性值较多），前端会将大部分属性放入一个JSON对象，并使用**Base64**编码后作为一个整体发送给后端，后端解码后可使用

### 返回值规范

为方便前后端通信，后端的所有返回值属性中至少包含一下两种情形之一：

```JSON
    {
        "result": true,
        //操作成功

        "msessage": ""
    }
```
或失败时返回如下值

```JSON
    {
        "result": false,
        //操作失败

        "msessage": "请在这里返回错误原因..."
    }
```



## 用户模块

### 用户注册
+ URL: ```/users```
+ 方法： ```POST```
+ 参数：
    
    |参数|描述|
    |:-|:-|
    |nickname|昵称|
    |email|邮箱|
    |phone|手机号码|
    |idCode|验证码|
    |passwordEncrypt|加密后的密码|

+ 返回值：
    ```JSON
        {
            "result": true,
            "message": "注册成功"
        }
    ```
### 用户登录
+ URL： ```/users```
+ 方法：```GET```
+ 参数：

    |参数|描述|
    |:-|:-|
    |email|邮箱|
    |passwordEncrypt|加密后的密码|

+ 返回值：
    ```JSON
        {
            "result": false,
            "message": "密码错误"
        }
    ```

### 获取当前登录用户
+ URL：```/users```
+ 方法：```GET```
+ 参数：无
+ 返回值：
    ```JSON
        {
            "result": true,
            "message": "",
            "email": "16302010012@fudan.edu.cn",
            "phone": "18717725112",
            "nickname": "xing"
        }
    ```