const mysql = require('mysql');
const config = require('./../config').database;

const pool = mysql.createPool({
    user: config.USERNAME,
    password: config.PASSWORD,
    database: config.DATABASE
});

let query = function(sql, values) {
    return new Promise((resolve, reject) => {
        pool.getConnection(function(err, connection) {
            if (err) {
                resolve(err);
            } else {
                connection.query(sql, values, (err, rows) => {
                    if (err) {
                        reject(err);
                    } else {
                        resolve(rows);
                    }
                });
                connection.release();
            }
        })
    })
};

let insert = function(table, values) {
    let sql = 'INSERT INTO ?? SET ?';
    return query(sql, [table, values]);
}

module.exports = {
    query
}